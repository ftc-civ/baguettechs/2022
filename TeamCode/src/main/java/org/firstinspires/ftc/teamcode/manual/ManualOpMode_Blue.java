package org.firstinspires.ftc.teamcode.manual;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp(name="Manual Blue", group="Main")

public class ManualOpMode_Blue extends ManualOpMode{
    public ManualOpMode_Blue(){
        super(Team.BLUE);
    }

}
