/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

// import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import static java.lang.Thread.sleep;

// @Config
public class Launcher
{
    public static  double HIGH_VALUE = 4350;
    public static double MEDIUM_VALUE = 3400;
    public static double LOW_VALUE = 2500;
    public static double difference = 1.8;

    private double launcherSpeed = HIGH_VALUE;
    private double defaultLauncherSpeed = 1.0;

    private double tiltVal = 0.0;
    private double minTilt = 0.0;
    private double maxTilt = 0.5;

    private boolean beltOn = false;

    private DcMotor launcher1;
    private DcMotor launcher2;

    private double lastLauncher1Pos = 0.0;
    private double lastLauncher2Pos = 0.0;
    private double lastTime = 0.0;

    private boolean launcherOn = false;

    private double realLauncher1Power = 0.0;
    private double realLauncher2Power = 0.0;

    private double realLauncher1Speed = 0.0;
    private double realLauncher2Speed = 0.0;

    private ElapsedTime runtime;

    private Servo tilt;

    private DcMotor belt;

    private Telemetry telemetry;

    public enum Target {
      POWERSHOT_1_RED,
      POWERSHOT_2_RED,
      POWERSHOT_3_RED,
      HIGH_GOAL_RED,
      MEDIUM_GOAL_RED,
      LOW_GOAL_RED,
      POWERSHOT_1_BLUE,
      POWERSHOT_2_BLUE,
      POWERSHOT_3_BLUE,
      HIGH_GOAL_BLUE,
      MEDIUM_GOAL_BLUE,
      LOW_GOAL_BLUE,
    };

    public Launcher(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor launcher1Motor, DcMotor launcher2Motor, Servo tiltServo, DcMotor beltMotor) {
        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        runtime = globalRuntime;

        // Assign servos
        launcher1 = launcher1Motor; launcher1.setDirection(DcMotor.Direction.REVERSE);
        launcher2 = launcher2Motor; launcher2.setDirection(DcMotor.Direction.FORWARD);

        tilt = tiltServo; tilt.setDirection(Servo.Direction.FORWARD);

        belt = beltMotor; belt.setDirection(DcMotor.Direction.REVERSE);
    }


    /* AUTOMATIC CONTROL */
    // Launches a donut
    public void launch(Target target) {
        return;
    }


    /* MANUAL CONTROL */
    // Sets tilt of launch ramp
    public void setTilt(double val) {
      tiltVal = val;
    }

    // Increases or decreases tilt of launch ramp by given value
    public void changeTilt(double change) {
      tiltVal = tiltVal + change;
    }

    // Sets state of launcher, with optional speed if on
    public void setLauncherSpeed(double speed) {
      launcherSpeed = speed;
    }

    public void setTarget(Target target) {
       switch (target) {
           case HIGH_GOAL_BLUE:
               launcherSpeed = HIGH_VALUE;
               break;
           case HIGH_GOAL_RED:
               launcherSpeed = HIGH_VALUE;
               break;
           case MEDIUM_GOAL_BLUE:
               launcherSpeed = MEDIUM_VALUE;
               break;
           case MEDIUM_GOAL_RED:
               launcherSpeed = MEDIUM_VALUE;
               break;
           case LOW_GOAL_BLUE:
               launcherSpeed = LOW_VALUE;
               break;
           case LOW_GOAL_RED:
               launcherSpeed = LOW_VALUE;
               break;
           default:
               launcherSpeed = MEDIUM_VALUE;
               break;
       }
    }

    public void setTargetGamepad(Gamepad gamepad) {
        if (gamepad.right_stick_y > 0.8) {
            launcherSpeed = LOW_VALUE;
        } else if (gamepad.right_stick_y < -0.8) {
            launcherSpeed = HIGH_VALUE;
        } else if (gamepad.right_stick_x > 0.8) {
            launcherSpeed = MEDIUM_VALUE;
        } else if (gamepad.right_stick_x < -0.8) {
            launcherSpeed = MEDIUM_VALUE;
        }
        telemetry.addData("Launcher Speed", launcherSpeed);
    }

    public void setLauncher(boolean state) {
      launcherOn = state;
    }

    // Toggles state of launcher, with optional speed if on
    public void toggleLauncher() {
      launcherOn = !launcherOn;
    }

    public void toggleBelt() {
      beltOn = !beltOn;
    }
    public void setBelt(boolean state) {
      beltOn = state;
    }

    public void reverseBelt() {
        belt.setPower(-1.0);
        try {
            sleep(100);
        } catch (InterruptedException e) {
            belt.setPower(0.0);
        }
        belt.setPower(0.0);
    }

    // Toggles both, depending on launcher (to avoid being out of sync)
    public void toggle() {
      beltOn = !launcherOn;
      launcherOn = !launcherOn;
    }


    /* UTIL */
    // Returns a value mapped from (0, 1) to (minTilt, maxTilt)
    private double interpolateTilt(double val) {
      return minTilt + (val*(maxTilt-minTilt));
    }

    // Sets power and position of motors
    public void apply() {
      if (tiltVal < minTilt) {
        tiltVal = minTilt;
      } else if (tiltVal > maxTilt) {
        tiltVal = maxTilt;
      }
      tilt.setPosition(interpolateTilt(tiltVal));

      // telemetry.addData("Launcher1", launcher1.getCurrentPosition());
      // telemetry.addData("Launcher2", launcher2.getCurrentPosition());

      double currentLauncher1Pos = launcher1.getCurrentPosition();
      double currentLauncher2Pos = launcher2.getCurrentPosition();

      double currentTime = runtime.time();
      if (launcherOn) {
        realLauncher1Speed = (currentLauncher1Pos - lastLauncher1Pos) / (currentTime - lastTime) * difference;
        realLauncher2Speed = (currentLauncher2Pos - lastLauncher2Pos) / (currentTime - lastTime);

        if (realLauncher1Speed > launcherSpeed) {
          realLauncher1Power = realLauncher1Power - 0.0001  * Math.abs(realLauncher1Speed - launcherSpeed); // * Math.abs(realLauncher1Speed - launcherSpeed);
        } else if (realLauncher1Speed < launcherSpeed) {
          realLauncher1Power = realLauncher1Power + 0.0001 * Math.abs(realLauncher1Speed - launcherSpeed); // * Math.abs(realLauncher1Speed - launcherSpeed);
        }
        telemetry.addData("Diff launcher1", 0.0001 * Math.abs(realLauncher1Speed - launcherSpeed));
        if (realLauncher2Speed > launcherSpeed) {
          realLauncher2Power = realLauncher2Power - 0.0001 * Math.abs(realLauncher2Speed - launcherSpeed); // * Math.abs(realLauncher2Speed - launcherSpeed);
        } else if (realLauncher1Speed < launcherSpeed) {
          realLauncher2Power = realLauncher2Power + 0.0001 * Math.abs(realLauncher2Speed - launcherSpeed); // * Math.abs(realLauncher2Speed - launcherSpeed);
        }
        telemetry.addData("Diff launcher2", 0.0001 * Math.abs(realLauncher2Speed - launcherSpeed));
        launcher1.setPower(realLauncher1Power);
        launcher2.setPower(realLauncher2Power);
      } else {
        launcher1.setPower(0.0);
        launcher2.setPower(0.0);
      }

      if (beltOn) {
        belt.setPower(1.0);
      } else {
        belt.setPower(0.0);
      }

       telemetry.addData("L1 speed", realLauncher1Speed);
       telemetry.addData("L2 speed", realLauncher2Speed);
       telemetry.addData("L1 power", realLauncher1Power);
       telemetry.addData("L2 power", realLauncher2Power);
       telemetry.addData("Belt", beltOn ? "on" : "off");
       telemetry.addData("Speed", launcherSpeed);

       lastTime = currentTime;
       lastLauncher1Pos = currentLauncher1Pos;
       lastLauncher2Pos = currentLauncher2Pos;

       // telemetry.addData("Launcher", "(%.2f), tilt (%.2f)", (launcherSpeed == 0.0) ? "off" : "on", String.valueOf(tilt));
    }
}
