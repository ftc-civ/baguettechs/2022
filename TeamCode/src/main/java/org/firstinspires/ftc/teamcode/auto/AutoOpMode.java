package org.firstinspires.ftc.teamcode.auto;

import androidx.annotation.NonNull;

import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.profile.VelocityConstraint;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.acmerobotics.roadrunner.trajectory.constraints.AngularVelocityConstraint;
import com.acmerobotics.roadrunner.trajectory.constraints.MinVelocityConstraint;
import com.acmerobotics.roadrunner.trajectory.constraints.TrajectoryVelocityConstraint;
import com.acmerobotics.roadrunner.trajectory.constraints.TranslationalVelocityConstraint;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.roadrunner.drive.DriveConstants;
import org.firstinspires.ftc.teamcode.roadrunner.drive.MainDrive;
import org.firstinspires.ftc.teamcode.robot.Cam;
import org.firstinspires.ftc.teamcode.robot.Grue;
import org.firstinspires.ftc.teamcode.robot.Movement;
import org.firstinspires.ftc.teamcode.robot.Carousel;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

// @Config
public class AutoOpMode extends LinearOpMode {

    ///////////////////////////////////////// Commandes pour se connecter au robot ////////////////////////////////////////////
    //cd C:\Users\nolha\AppData\Local\Android\Sdk\platform-tools
    //adb kill-server
    //adb connect 192.168.43.1:5555

    ///////////////////////////////////////// Variables ////////////////////////////////////////////
    ////////////////////////////////// Public Variables //////////////////////////////////
    public Grue grue;
    public Carousel carousel;
    public Cam.Etage etage;
    public Programm programm;
    public VuforiaLocalizer vuforia;
    public TFObjectDetector tfod;
    public TelemetrySystem telemetrysystem;
    public Cam object1;
    public MainDrive drive;

    public enum Programm {
        BLUE_C,
        BLUE_W,
        RED_C,
        RED_W
    }

    public enum CameraObject {
        NONE,
        DUCK,
        CUBE,
        BALL,
        UNSURE
    }

    private enum TelemetrySystem {
        INITIALAZING,
        READY,
        RECOGNITION,
        MOVING
    }


    ////////////////////////////////// Private variables //////////////////////////////////
    private ElapsedTime runtime = new ElapsedTime();
    private boolean telemetry_debug_on = false;
    private boolean telemetry_system_on = false;
    private boolean telemetry_cam_on = false;
    private boolean telemetry_ARM_on = false;
    ////////////////////////////////// New variables //////////////////////////////////
    Cam.Etage object_to_detect = Cam.Etage.ETAGE_UNSURE;
    int iter = 0;

    Trajectory M1_1;
    Trajectory M1_15;
    Trajectory M1_2;
    Trajectory M1_3;
    Trajectory M1_4;
    Trajectory M1_5;
    Trajectory M1_6;

    Trajectory M2_1;
    Trajectory M2_2;
    Trajectory M2_spe;

    Trajectory M3_1;
    Trajectory M3_15;
    Trajectory M3_2;
    Trajectory M3_3;
    Trajectory M3_4;
    Trajectory M3_5;
    Trajectory M3_6;

    Trajectory M4_1;
    Trajectory M4_2;
    Trajectory M4_spe;


    ////////////////////////////////// Constructors //////////////////////////////////

    public AutoOpMode(AutoOpMode.Programm initProgramm) {
        programm = initProgramm;
    }

    ///////////////////////////////////////// METHODS /////////////////////////////////////////

    ///////////////////////////////// Telemetry methods /////////////////////////////////
    public void telemetry_debug() {

            Telemetry.Item debug = telemetry.addLine()
                    .addData("DEBUG", "");

        telemetry.addLine()
                .addData("System", "")
                .addData("Step", telemetrysystem);

        telemetry.addLine()
                .addData("Movement", "")
                .addData("Trajectoire", "");

        telemetry.addLine()
                .addData("Camera", " ")
                //.addData("Object", object_to_detect)
                .addData("Recognition", object1)
                .addData("Iterations", iter);

        telemetry.addLine()
                .addData("ARM", " ")
                .addData("bras", grue.target)
                .addData("Encodergrue", grue.targetPositionInt)
                .addData("Servo", grue.state);

    if (telemetry_debug_on == false) {
        telemetry.removeItem(debug);
    }
    }

    public void telemetry_camera(){
        Telemetry.Item Camera = telemetry.addLine()
                .addData("Camera :", " ")
                //.addData("Object :", object_to_detect)
                .addData("Recognition :", object1)
                .addData("Iterations :", iter);
        if (telemetry_cam_on == false) {
            telemetry.removeItem(Camera);
        }
    }


    public void telemetry_system(){
        Telemetry.Item sys =telemetry.addLine()
                .addData("System", "")
                .addData("programm", programm)
                .addData("Step", telemetrysystem);
        if (telemetry_system_on == false) {
            telemetry.removeItem(sys);
        }
    }

    public void telemetry_manager(){
        telemetry_system();
        telemetry_camera();
        grue.telemetry_ARM();
        telemetry.update();
    }

    public void sleepFor(double millis) {
        //telemetry.update();
        double startTime = runtime.time();
        while (runtime.time() - startTime < (millis / 1000)) {
            if (!opModeIsActive()) {
                break;
            }
        }
    }

    ///////////////////////////////// Trajectories /////////////////////////////////
    ///////////////////////////////// Objects /////////////////////////////////

    public void init_objet(){
        drive =  new MainDrive(hardwareMap);

        object1 = new Cam(
                telemetry,
                runtime,
                hardwareMap.get(WebcamName.class, "Webcam 1"),
                hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName())
        );

        grue = new Grue (
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "grue"),
                hardwareMap.get(Servo.class, "basculator_servo"),
                hardwareMap.get(DcMotor.class, "recuperation")

        ) ;

        carousel = new Carousel (
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "Carousel")
        );
    }

    public void init_roadrunner(){
        switch (programm){
            case RED_W:
                Pose2d myPose1 = new Pose2d(12, -65, Math.toRadians(90));
                drive.setPoseEstimate(myPose1);
                M1_1 = drive.trajectoryBuilder(myPose1)
                        .lineToSplineHeading(new Pose2d(-12, -45, Math.toRadians(90)))
                        .build();
                M1_15 = drive.trajectoryBuilder(myPose1)
                        .lineToSplineHeading(new Pose2d(17, -63, Math.toRadians(90)))
                        .build();
                M1_2 = drive.trajectoryBuilder(myPose1)
                        .lineToSplineHeading(new Pose2d(60, -63, Math.toRadians(90)),
                        new MinVelocityConstraint(Arrays.asList(new TranslationalVelocityConstraint(15))),
                        null)
                        .build();

                M1_3 = drive.trajectoryBuilder(myPose1)
                        .splineTo(new Vector2d(-10, -63), 0)
                        .build();

                M1_4 = drive.trajectoryBuilder(myPose1)
                        .splineTo(new Vector2d(-10, -40), 0)
                        .build();

                M1_5 = drive.trajectoryBuilder(myPose1)
                        .splineTo(new Vector2d(-10, -63), 0)
                        .build();

                M1_6 = drive.trajectoryBuilder(myPose1)
                        .splineTo(new Vector2d(60, -63), 0)
                        .build();

                break;
            case RED_C:
                Pose2d myPose2 = new Pose2d(-35, -65, Math.toRadians(90));
                drive.setPoseEstimate(myPose2);
                M2_1 = drive.trajectoryBuilder(myPose2)
                        .lineToSplineHeading(new Pose2d(-63.5, -63, Math.toRadians(90)))
                        .build();
                M2_2 = drive.trajectoryBuilder(myPose2)
                        .lineToSplineHeading(new Pose2d(-15, -40, Math.toRadians(90)))
                        .build();
                M2_spe = drive.trajectoryBuilder(myPose2)
                        .lineToSplineHeading(new Pose2d(-66, -36, Math.toRadians(90)))
                        .build();
                break;
            case BLUE_W:
                Pose2d myPose3 = new Pose2d(12, 65, Math.toRadians(270));
                drive.setPoseEstimate(myPose3);
                M3_1 = drive.trajectoryBuilder(myPose3)
                        .lineToSplineHeading(new Pose2d(-12, 45, Math.toRadians(270)))
                        .build();

                M3_15 = drive.trajectoryBuilder(myPose3)
                    .lineToSplineHeading(new Pose2d(17, 63, Math.toRadians(270)))
                    .build();

                M3_2 = drive.trajectoryBuilder(myPose3)
                        .lineToSplineHeading(
                                new Pose2d(60, 63, Math.toRadians(270)),
                                new MinVelocityConstraint(Arrays.asList(new TranslationalVelocityConstraint(15))),
                                null)
                        .build();

                M3_3 = drive.trajectoryBuilder(myPose3)
                        .splineTo(new Vector2d(-10, 63), 0)
                        .build();

                M3_4 = drive.trajectoryBuilder(myPose3)
                        .splineTo(new Vector2d(-10, 40), 0)
                        .build();

                M3_5 = drive.trajectoryBuilder(myPose3)
                        .splineTo(new Vector2d(-10, 63), 0)
                        .build();

                M3_6 = drive.trajectoryBuilder(myPose3)
                        .splineTo(new Vector2d(60, 63), 0)
                        .build();
                break;
            case BLUE_C:
                Pose2d myPose4 = new Pose2d(-35, 65, Math.toRadians(270));
                drive.setPoseEstimate(myPose4);
                M4_1 = drive.trajectoryBuilder(myPose4)
                        .lineToSplineHeading(new Pose2d(-63.5, 63, Math.toRadians(270)))
                        .build();

                M4_2 = drive.trajectoryBuilder(myPose4)
                        .lineToSplineHeading(new Pose2d(-15, -40, Math.toRadians(270)))
                        .build();
                M4_spe = drive.trajectoryBuilder(myPose4)
                        .lineToSplineHeading(new Pose2d(-66, 36, Math.toRadians(270)))
                        .build();
                break;
        }
    }


    public void M_1(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_1);
                Pose2d myPose1 = new Pose2d(-12, -45, Math.toRadians(90));
                break;
            case RED_C:
                drive.followTrajectory(M2_1);
                Pose2d myPose2 = new Pose2d(-63.5, -63, Math.toRadians(90));
                break;
            case BLUE_W:
                drive.followTrajectory(M3_1);
                Pose2d myPose3 = new Pose2d(-12, 45, Math.toRadians(270));
                break;
            case BLUE_C:
                drive.followTrajectory(M4_1);
                Pose2d myPose4 = new Pose2d(-63.5, 63, Math.toRadians(270));
                break;
        }
    }
    public void M_15(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_15);
                Pose2d myPose1 = new Pose2d(17, -66, Math.toRadians(90));
                break;
            case RED_C:
                break;
            case BLUE_W:
                drive.followTrajectory(M3_15);
                Pose2d myPose3 = new Pose2d(17, 66, Math.toRadians(270));
                break;
            case BLUE_C:
                break;
        }
    }

    public void M_2(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_2);
                Pose2d myPose1 = new Pose2d(60, -66, Math.toRadians(90));
                break;
            case RED_C:
                drive.followTrajectory(M2_2);
                Pose2d myPose2 = new Pose2d(-30, -25, Math.toRadians(90));
                break;
            case BLUE_W:
                drive.followTrajectory(M3_2);
                Pose2d myPose3 = new Pose2d(60, 66, Math.toRadians(270));
                break;
            case BLUE_C:
                drive.followTrajectory(M4_2);
                Pose2d myPose4 = new Pose2d(-30, 25, Math.toRadians(270));
                break;
        }
    }

    public void M_3(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_3);
                Pose2d myPose1 = new Pose2d(-10, -63, Math.toRadians(90));
                break;
            case RED_C:
                break;
            case BLUE_W:
                drive.followTrajectory(M3_3);
                Pose2d myPose3 = new Pose2d(-10, 63, Math.toRadians(270));
                break;
            case BLUE_C:
                break;
        }
    }

    public void M_4(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_4);
                Pose2d myPose1 = new Pose2d(-10, -40, Math.toRadians(90));
                break;
            case RED_C:
                break;
            case BLUE_W:
                drive.followTrajectory(M3_4);
                Pose2d myPose3 = new Pose2d(-10, 40, Math.toRadians(270));
                break;
            case BLUE_C:
                break;
        }
    }

    public void M_5(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_5);
                Pose2d myPose1 = new Pose2d(-10, -63, Math.toRadians(90));
                break;
            case RED_C:
                break;
            case BLUE_W:
                drive.followTrajectory(M3_5);
                Pose2d myPose3 = new Pose2d(-10, 63, Math.toRadians(270));
                break;
            case BLUE_C:
                break;
        }
    }

    public void M_6(){
        switch(programm){
            case RED_W:
                drive.followTrajectory(M1_6);
                Pose2d myPose1 = new Pose2d(60, -63, Math.toRadians(90));
                break;
            case RED_C:
                break;
            case BLUE_W:
                drive.followTrajectory(M3_6);
                Pose2d myPose3 = new Pose2d(60, 63, Math.toRadians(270));
                break;
            case BLUE_C:
                break;
        }
    }

    public void M_spe(){
        switch(programm){
            case RED_W:
                break;
            case RED_C:
                drive.followTrajectory(M2_spe);
                Pose2d myPose2 = new Pose2d(-66, -36, Math.toRadians(90));
                break;
            case BLUE_W:
                break;
            case BLUE_C:
                drive.followTrajectory(M4_spe);
                Pose2d myPose4 = new Pose2d(-66, 36, Math.toRadians(270));
                break;
        }
    }

    ///////////////////////////////// Functions /////////////////////////////////

    public void Init(){
        init_objet();
        telemetry_system_on= true; telemetrysystem = TelemetrySystem.INITIALAZING;
        telemetry_manager();
        init_roadrunner();
        object1.activate();
        grue.pos();
        grue.apply();

         telemetrysystem = TelemetrySystem.READY;telemetry_manager();
    }

    public void recognition(){
         telemetrysystem = TelemetrySystem.RECOGNITION;telemetry_manager();
        telemetry_cam_on = true; telemetry_manager();

        double startCheckTime = runtime.time();

        if (tfod == null) {
            telemetry.addData("null?", "oui");
        }

        while (object_to_detect == Cam.Etage.ETAGE_UNSURE && (runtime.time() - startCheckTime < 2)) {
            iter+=1;
            object_to_detect = object1.getPosition();
            if (!opModeIsActive()) return;
        }
    }

    public void setEtage(){
        telemetry_manager();
        switch (object_to_detect) {
            case ETAGE_1:
                grue.lvl1();
                grue.apply();
                break;
            case ETAGE_2:
                grue.lvl2();
                grue.apply();
                break;
            case ETAGE_3:
                grue.lvl3();
                grue.apply();
                break;
            case ETAGE_UNSURE:
                grue.lvl3();
                grue.apply();

        }
        telemetry_cam_on = false; telemetry_manager();
        telemetry_ARM_on = false; telemetry_manager();

    }

    public void action(){
        telemetrysystem = TelemetrySystem.MOVING;telemetry_manager();

        switch (programm) {
            case BLUE_C:
                M_1();
                carousel.work();
                sleepFor(5000);
                carousel.stop();

                setEtage();
                M_2();
                while (grue.finished() == false ){
                    grue.finished();
                    if(!opModeIsActive()){
                        break;
                    }
                }
                sleepFor(3000);
                M_spe();
                grue.lvl0();
                break;

            case BLUE_W:
                setEtage();
                M_1();
                while (grue.finished() == false ){
                    grue.finished();
                    if(!opModeIsActive()){
                        break;
                    }
                }
                sleepFor(10000);
                M_15();
                grue.lvl0();grue.action();grue.apply();
                sleepFor(1000);
                M_2();

                /*
                sleepFor(10000);
                M_3();
                M_4();

                 */
                break;

            case RED_C:
                M_1();
                carousel.workent();
                sleepFor(5000);
                carousel.stop();

                setEtage();
                M_2();
                while (grue.finished() == false ){
                    grue.finished();
                    if(!opModeIsActive()){
                        break;
                    }
                }
                sleepFor(3000);
                M_spe();
                grue.lvl0();
                break;

            case RED_W:
                setEtage();
                M_1();
                while (grue.finished() == false ){
                    grue.finished();
                    if(!opModeIsActive()){
                        break;
                    }
                }
                sleepFor(3000);
                M_15();
                grue.lvl0();grue.action();grue.apply();
                sleepFor(1000);
                M_2();

                /*
                sleepFor(10000);
                M_3();
                M_4();
                */

                break;
        }
    }
    @Override
    public void runOpMode() {// telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());
        ///////////////////////////////////////// INIT /////////////////////////////////////////
        Init();
        waitForStart();
        ///////////////////////////////////////// RUN /////////////////////////////////////////
        recognition();
        action();

    }
}