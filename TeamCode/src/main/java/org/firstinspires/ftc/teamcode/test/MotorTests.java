package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;

@TeleOp(name="Motor Tests", group="test")
public class MotorTests extends LinearOpMode
{
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive, recuperation, carousel, gruemotor;
    private Servo servo;
    private void sleepT(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void runOpMode() {
        frontLeftDrive = hardwareMap.get(DcMotor.class, "front_left_drive");
        frontRightDrive = hardwareMap.get(DcMotor.class, "front_right_drive");
        backLeftDrive = hardwareMap.get(DcMotor.class, "back_left_drive");
        backRightDrive = hardwareMap.get(DcMotor.class, "back_right_drive");
        recuperation = hardwareMap.get(DcMotor.class, "recuperation");
        carousel = hardwareMap.get(DcMotor.class, "Carousel");
        gruemotor = hardwareMap.get(DcMotor.class, "grue");
        servo =hardwareMap.get(Servo.class, "basculator_servo");


        frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        backRightDrive.setDirection(DcMotor.Direction.FORWARD);
        recuperation.setDirection(DcMotor.Direction.FORWARD);
        carousel.setDirection(DcMotor.Direction.FORWARD);
        gruemotor.setDirection(DcMotor.Direction.FORWARD);

        telemetry.addData("Motor:", "front left");
        telemetry.update();
        frontLeftDrive.setPower(1.0);
        sleepT(3000);
        frontLeftDrive.setPower(0.0);
        telemetry.addData("Motor:", "front right");
        telemetry.update();
        frontRightDrive.setPower(1.0);
        sleepT(3000);
        frontRightDrive.setPower(0.0);
        telemetry.addData("Motor:", "back left");
        telemetry.update();
        backLeftDrive.setPower(1.0);
        sleepT(3000);
        backLeftDrive.setPower(0.0);
        telemetry.addData("Motor:", "back right");
        telemetry.update();
        backRightDrive.setPower(1.0);
        sleepT(3000);
        backRightDrive.setPower(0.0);
        telemetry.addData("Motor:", "recupération");
        telemetry.update();
        recuperation.setPower(1.0);
        sleepT(3000);
        recuperation.setPower(0.0);
        telemetry.addData("Motor:", "carousel");
        telemetry.update();
        carousel.setPower(1.0);
        sleepT(3000);
        carousel.setPower(0.0);
        telemetry.addData("Motor:", "grue");
        telemetry.update();
        gruemotor.setPower(0.1);
        sleepT(3000);
        gruemotor.setPower(0.0);
    }

    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////
}
