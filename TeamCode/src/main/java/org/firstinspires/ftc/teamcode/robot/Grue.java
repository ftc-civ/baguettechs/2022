package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;
import org.firstinspires.ftc.teamcode.robot.Cam;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class Grue {

    ///////////////////////////////////////// Variables ////////////////////////////////////////////

    ////////////////////////////////// Private Variables //////////////////////////////////
    private ElapsedTime runtime;
    private Cam cam;
    private DcMotor grueMotor;
    private Servo depositMotor;
    private DcMotor ticksMotor;
    private Telemetry telemetry;

    private double gruePower = 0;
    private int grueState = 0;
    public static double state = 1;
    private boolean servostate = false;
    private double recupPower = 0;

    private boolean isTriggered = false;
    private boolean isTriggered2 = false;
    private boolean buttonpressed = false;
    private boolean Ibuttonpressed = false;

    private boolean gotThere = false;
    private boolean gotThere1 = false;
    private boolean gotThere2 = false;
    private boolean gotThere3 = false;
    private boolean finishing = false;

    private boolean telemetry_ARM_on = true;
    private ArmPos lastTarget = ArmPos.INIT_POS;
    ////////////////////////////////// Public Variables //////////////////////////////////
    public enum ArmPos {INIT_POS, POS_1, POS_2, POS_3,POS_UP, OTHER}
    public ArmPos target = ArmPos.INIT_POS;


    public int targetPositionInt = 0;


    ////////////////////////////////// Constructors //////////////////////////////////
    public Grue(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor GlobalGrue, Servo GlobalDeposit, DcMotor ticksMotorGlobal) {

        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        runtime = globalRuntime;

        // Assign motors from ManualOpMode
        grueMotor = GlobalGrue;
        grueMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        grueMotor.setTargetPosition(0);
        grueMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        grueMotor.setDirection(DcMotor.Direction.FORWARD);

        depositMotor = GlobalDeposit;
        depositMotor.setDirection(Servo.Direction.FORWARD);

        ticksMotor = ticksMotorGlobal;
        ticksMotor.setDirection(DcMotor.Direction.FORWARD);
    }


    ///////////////////////////////////////// METHODS /////////////////////////////////////////
    // Turns off all motors, full stop

    public void telemetry_ARM(){
        Telemetry.Item ARM = telemetry.addLine()
                .addData("ARM", " ")
                .addData("bras", target)
                .addData("Encodergrue", grueState)
                .addData("Servo", state);
        if (telemetry_ARM_on == false) {
            telemetry.removeItem(ARM);
        }
    }
    public void reset(boolean includeTurn) {
        gruePower = 0;
        recupPower= 0;
    }

    public void reset() {
        reset(true);
    }

    ///////////////////////////////////////// RECUPERATION /////////////////////////////////////////
    public void  marche() {
        if (buttonpressed == false) {
            recupPower  = 1;
            buttonpressed = true;
            grueState = grueState -20;
        }
        else {
            recupPower  = 0;
            buttonpressed = false;
            grueState = grueState +20;
        }
    }
    public void inverse() {
        if (Ibuttonpressed == false) {
            recupPower = -1;
            Ibuttonpressed = true;
            grueState = grueState -30;
        } else {
            recupPower = 0;
            Ibuttonpressed = false;
            grueState = grueState +30;
        }
    }
    ///////////////////////////////////////// GRUE /////////////////////////////////////////

    ////////////// ManualOpMode ////////////////////
    public void Gamepad_step(Gamepad gamepad) {
        if (gamepad.right_trigger !=0){
            if (isTriggered == false){
                lvl3();
                }
            isTriggered = true;
        }
        if (gamepad.right_trigger ==0){
            isTriggered = false;
        }
        if (gamepad.left_trigger !=0){
            if (isTriggered2 == false) {
                lvl0();
            }
            isTriggered2 = true;
        }
        if (gamepad.right_trigger ==0){
            isTriggered2 = false;
        }
    }

    public void Gamepad_step2(Gamepad gamepad2) {
        if (gamepad2.right_trigger !=0){
            if (isTriggered == false){
                lvl3();
            }
            isTriggered = true;
        }
        if (gamepad2.right_trigger ==0){
            isTriggered = false;
        }
        if (gamepad2.left_trigger !=0){
            if (isTriggered2 == false) {
                lvl0();
            }
            isTriggered2 = true;
        }
        if (gamepad2.right_trigger ==0){
            isTriggered2 = false;
        }
    }

    public void armMonte(){
        grueState = grueState + 30;
        lastTarget = ArmPos.OTHER;
        target = lastTarget;
    }

    public void armDescend(){
        grueState = grueState - 30;
        lastTarget = ArmPos.OTHER;
        target = lastTarget;

    }

    ///////////////////////// AutoOpMode ////////////////////
    public void lvl1(){ lastTarget = ArmPos.POS_1;
        action();}
    public void lvl2(){
        lastTarget = ArmPos.POS_2;action();
    }
    public void lvl3(){
        lastTarget = ArmPos.POS_3;action();
    }
    public void lvl0(){
        lastTarget = ArmPos.INIT_POS;action();
    }
    public void go_up (){
        lastTarget = ArmPos.POS_UP;
        action();
    }

    public void setTarget(ArmPos armpos) {
        target = armpos;
    }

    public void action() {
        target = lastTarget;
        if (target == ArmPos.INIT_POS){
            gotThere = true;
        } else {
            gotThere = false;
        }
        if (target == ArmPos.POS_1){
            gotThere1 = true;
        } else {
            gotThere1 = false;
        }
        if (target == ArmPos.POS_2){
            gotThere2 = true;
        } else {
            gotThere2 = false;
        }
        if (target == ArmPos.POS_3){
            gotThere3 = true;
        } else {
            gotThere3 = false;
        }
    }

    ///////////////////////////////////////// SERVO /////////////////////////////////////////
    public void pos() {
        if (servostate == false) {
            depositMotor.setDirection(Servo.Direction.FORWARD);
            state=1;
            servostate = true;
        } else {
            depositMotor.setDirection(Servo.Direction.FORWARD);
            if (grueState > 1000){
                state=0.45;
            }
             else if (grueState > 785){
                state=0.35;
            }
            else if (grueState > 650){
                state=0.425;
            }
            else {state=0.6;}
            servostate = false;
        }
    }
    public boolean finished(){
        switch (target){
            case INIT_POS:
                state = 1;
                break;
            case POS_1:
                if (grueMotor.getCurrentPosition()  > 500){
                    state = 0.6;
                    finishing = true;
                } else {state=1;
                    finishing = false;
                }
                break;
            case POS_2:
                if (grueMotor.getCurrentPosition()  > 650){
                    state = 0.425;
                    finishing = true;
                } else {
                    state=1;
                    finishing = false;}
                break;

            case POS_3:
                if (grueMotor.getCurrentPosition()  > 790){
                    state = 0.35;
                    finishing = true;
                } else {
                    state=1;
                    finishing = false;
                }
                break;
            case POS_UP:
                if (grueMotor.getCurrentPosition()  > 900){
                    state = 0.35;
                    finishing = true;
                } else {
                    state=1;
                    finishing = false;
                }
                break;
        }
        depositMotor.setPosition(state);
        return(finishing);
    }

    /* GET INFO */

    public int getEncoder() {
        return grueMotor.getCurrentPosition();
    }
    public int getTEncoder(){return ticksMotor.getCurrentPosition();}

    /* APPLY CHANGES */

    // Apply all changes made before
    public void apply() {
        /* SERVO */
        depositMotor.setPosition(state);

        /* GRUE */
        if (target == ArmPos.INIT_POS) {
            if (state != 1){
                state = 1;
            }
            grueState = 45;

            if (gotThere == true) {
                recupPower = 0.5;
                if (grueMotor.getCurrentPosition()  < 45) {
                    recupPower = 0;
                    gotThere = false;
                }
            }
        } else if (target == ArmPos.POS_1) {
            grueState = 593;
            if (gotThere1 == true) {
                recupPower = -0.5;
                if (grueMotor.getCurrentPosition() > 400) {
                    recupPower = 0;
                    gotThere1 = false;
                }
            }
        } else if (target == ArmPos.POS_2) {
            grueState =747;
            if (gotThere2 == true) {
                recupPower = -0.5;
                if (grueMotor.getCurrentPosition() > 400) {
                    recupPower = 0;
                    gotThere2 = false;
                }
            }

        } else if (target == ArmPos.POS_3) {
            grueState =870;
            if (gotThere3 == true) {
                recupPower = -0.5;
                if (grueMotor.getCurrentPosition() > 400) {
                    recupPower = 0;
                    gotThere3 = false;
                }
            }
        }
        else if (target == ArmPos.POS_UP) {
            grueState =970;
            if (gotThere3 == true) {
                recupPower = -0.5;
                if (grueMotor.getCurrentPosition() > 400) {
                    recupPower = 0;
                    gotThere3 = false;
                }
            }
        } else if (target == ArmPos.OTHER){
            grueMotor.setTargetPosition(grueState);
        }

        /* Apply changes */
        grueMotor.setTargetPosition(grueState);
        grueMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        grueMotor.setPower(0.3);
        ticksMotor.setPower(Range.clip(recupPower, -1.0, 1.0));

    }

}

