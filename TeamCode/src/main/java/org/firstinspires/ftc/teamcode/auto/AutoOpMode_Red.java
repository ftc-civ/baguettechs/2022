package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.manual.ManualOpMode;

@Autonomous(name= "AutoOpMode Red C", group="Main")
public class AutoOpMode_Red extends AutoOpMode {

    public AutoOpMode_Red(){ super(Programm.RED_C);
    }
}
