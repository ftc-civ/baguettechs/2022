package org.firstinspires.ftc.teamcode.manual;
// Classes importées afin de pouvoir utiliser leurs fontions
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Carousel;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.GamepadController2;
import org.firstinspires.ftc.teamcode.robot.Grue;
import org.firstinspires.ftc.teamcode.robot.Movement;

import static java.lang.Thread.sleep;

public class ManualOpMode extends OpMode
{

    ///////////////////////////////////////// Commandes pour se connecter au robot ////////////////////////////////////////////
    //cd C:\Users\nolha\AppData\Local\Android\Sdk\platform-tools
    //adb kill-server
    //adb connect 192.168.43.1:5555

    ///////////////////////////////////////// Variables ////////////////////////////////////////////
    ////////////////////////////////// Public Variables //////////////////////////////////
    public Movement movement;
    public Carousel carousel;
    public Grue grue;
    public GamepadController gamepad;
    public GamepadController2 gamepadX;
    public Team team;
    public TelemetrySystem telemetrysystem;

    public enum Team {
        BLUE,
        RED
    }
    private enum TelemetrySystem {
        INITIALAZING,
        READY,
        ARM_MOVING,
        COLLECTING_OBJECT,
        DEPOSIT_OBJECT,
        CAROUSEL
    }

    ////////////////////////////////// Private variables //////////////////////////////////
    private ElapsedTime runtime = new ElapsedTime();
    private boolean telemetry_debug_on = true;
    private boolean telemetry_system_on = true;

    ////////////////////////////////// New variables //////////////////////////////////
    private double lastRuntime = 0.0;
    public boolean deux_gamepad = true;


    ////////////////////////////////// Constructors //////////////////////////////////

    public ManualOpMode (Team initTeam){
        team = initTeam;
    }

    ////////////////////////////////////////// METHODS /////////////////////////////////////////


    public void telemetry_debug() {

        Telemetry.Item debug = telemetry.addLine()
                .addData("DEBUG", "");

        telemetry.addLine()
                .addData("System", "")
                .addData("Step", telemetrysystem);

        telemetry.addLine()
                .addData("Movement", "")
                .addData("Trajectoire", "");

        telemetry.addLine()
                .addData("ARM", " ")
                .addData("bras", grue.target)
                .addData("Encodergrue", grue.targetPositionInt)
                .addData("Servo", grue.state);

        if (telemetry_debug_on == false) {
            telemetry.removeItem(debug);
        }
    }

    public void telemetry_system(){
        Telemetry.Item sys =telemetry.addLine()
                .addData("System", "")
                .addData("team", team)
                .addData("Step", telemetrysystem)
                .addData("Gamepads", deux_gamepad);
        if (telemetry_system_on == false) {
            telemetry.removeItem(sys);
        }
    }



    public void init_object(){
        movement = new Movement(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "front_left_drive"),//wtf is hardware map?
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive"),
                false );


        carousel = new Carousel (
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "Carousel")

        ) ;

        grue = new Grue (
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "grue"),
                hardwareMap.get(Servo.class, "basculator_servo"),
                hardwareMap.get(DcMotor.class, "recuperation")

        ) ;

        gamepad = new GamepadController(
                telemetry,
                runtime,
                gamepad1
        );

        gamepadX = new GamepadController2(
                telemetry,
                runtime,
                gamepad2
        );
    }
    public void system_functions() {
        if (
                (gamepadX.press(GamepadController2.Button.OPTIONS) && gamepadX.press(GamepadController2.Button.HOME))
                || (gamepad.press(GamepadController.Button.OPTIONS) && gamepad.press(GamepadController.Button.HOME))
        ) {
            if (deux_gamepad == false){
            deux_gamepad = true;

        }else if (deux_gamepad == true){
                deux_gamepad = false;

    }}}

    public void ARM_functions(){
        if (deux_gamepad == false){
            grue.Gamepad_step(gamepad1);
        } else { grue.Gamepad_step2(gamepad2);}
        if (deux_gamepad == true){
        if (gamepadX.press(GamepadController2.Button.RIGHT_BUMPER)) {
            grue.lvl1();
        }

        if (gamepadX.press(GamepadController2.Button.LEFT_BUMPER)) {
            grue.lvl2();
        }

        if ( gamepadX.press(GamepadController2.Button.A)) {
            grue.pos();
        }
        if (gamepadX.press(GamepadController2.Button.OPTIONS) && !gamepadX.press(GamepadController2.Button.HOME)) {
            grue.armMonte();
        }

        if ( gamepadX.press(GamepadController2.Button.SHARE)) {
            grue.armDescend();
        }
        if (gamepadX.press(GamepadController2.Button.LEFT_STICK)){
            grue.go_up();
        }}
        else {
            if (gamepad.press(GamepadController.Button.RIGHT_BUMPER)) {
                grue.lvl1();
            }

            if (gamepad.press(GamepadController.Button.LEFT_BUMPER)) {
                grue.lvl2();
            }

            if ( gamepad.press(GamepadController.Button.A)) {
                grue.pos();
            }
            if (gamepad.press(GamepadController.Button.OPTIONS) && !gamepad.press(GamepadController.Button.HOME)) {
                grue.armMonte();
            }

            if ( gamepad.press(GamepadController.Button.SHARE)) {
                grue.armDescend();
            }
            if (gamepad.press(GamepadController.Button.LEFT_STICK)){
                grue.go_up();
        }}
    }

    public void collecting_system(){
        if (gamepad.press(GamepadController.Button.B)) {
            grue.marche();
        }

        if (gamepad.press(GamepadController.Button.X)) {
            grue.inverse();
        }
    }

    public void carousel_function(){
        // Button Y : Carousel switch cats are cute//
        if (gamepad.press(GamepadController.Button.Y)) {
            if (team == Team.BLUE) {
                carousel.marche();
            }
            if (team == Team.RED) {
                carousel.inverse();
            }
        }
    }

    public void movement_functions(){
        if (movement.motorsEnabled) {
            movement.joystickTranslate(gamepad1);
            movement.dpadTranslate(gamepad1);
        }
        if (gamepad.press(GamepadController.Button.HOME) && !gamepad.press(GamepadController.Button.OPTIONS)){
            movement.putBrake();
        }
        }

    public void apply(){
        // Apply movements from all classes //
        movement.keepRotating();
        movement.apply();
        carousel.apply();
        grue.apply();
        /* CHORES */
        lastRuntime = runtime.time();
    }

    ///////////////////////////////////////// INIT /////////////////////////////////////////
    @Override
    public void init() {
        telemetrysystem = TelemetrySystem.INITIALAZING;
        init_object();
        grue.apply();
        grue.pos();
        telemetrysystem = TelemetrySystem.READY;

    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }

    @Override
    public void loop() {
        movement.reset();
        telemetry.update();

        // Calling methods to move the robot //
        system_functions();
        ARM_functions();
        collecting_system();
        carousel_function();
        movement_functions();

        ///////////////////////////////////////// RUN /////////////////////////////////////////
        apply();
        telemetry_system();
        movement.telemetry_movement();
        grue.telemetry_ARM();

    }

    ///////////////////////////////////////////////////////// STOP METHOD /////////////////////////////////////////////////////////
    @Override
    public void stop() {
    }
}
