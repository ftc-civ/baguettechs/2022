package org.firstinspires.ftc.teamcode.manual;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@TeleOp(name= "Manual Red", group="Main")
public class ManualOpMode_Red extends ManualOpMode{

    public ManualOpMode_Red (){
        super(Team.RED);
    }
}
