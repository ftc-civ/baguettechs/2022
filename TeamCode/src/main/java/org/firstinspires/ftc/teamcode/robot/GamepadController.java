/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class GamepadController
{

    private double longPressTime = 1.0;
    private Telemetry telemetry;
    private Gamepad gamepad;
    private ElapsedTime runtime;

    // I've not found any way to do this nicely, so it's just a separate variable for every button
    private boolean lastA = false;
    private boolean lastB = false;
    private boolean lastX = false;
    private boolean lastY = false;
    private boolean lastLeftStick = false;
    private boolean lastRightStick = false;
    private boolean lastLeftBumper= false;
    private boolean lastRightBumper = false;
    private boolean lastOptions = false;
    private boolean lastRightTrigger = false;
    private boolean lastLeftTrigger = false;
    private boolean lastShare = false;
    private boolean lastHome =false;



    // Store the last time at which A was not pressed
    private double lastNotATime = Double.POSITIVE_INFINITY;
    private double lastNotBTime = Double.POSITIVE_INFINITY;
    private double lastNotXTime = Double.POSITIVE_INFINITY;
    private double lastNotYTime = Double.POSITIVE_INFINITY;
    private double lastNotOptionTime = Double.POSITIVE_INFINITY;
    private double lastNotShareTime = Double.POSITIVE_INFINITY;

    public enum Button {
        X,
        Y,
        A,
        B,
        LEFT_STICK,
        RIGHT_STICK,
        LEFT_BUMPER,
        RIGHT_BUMPER,
        OPTIONS,
        RIGHT_TRIGGER,
        LEFT_TRIGGER,
        SHARE,
        HOME,

    }

    public GamepadController(Telemetry globalTelemetry, ElapsedTime globalRuntime, Gamepad globalGamepad) {
        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;
        gamepad = globalGamepad;
        runtime = globalRuntime;
    }

    // Get if a button is pressed, only returns true once
    public boolean press(Button button) {
      boolean pressed = false;
      switch (button) {
        case A:
          pressed = gamepad.a && !lastA;
          lastA = gamepad.a;
          break;

        case B:
          pressed = gamepad.b && !lastB;
          lastB = gamepad.b;
          break;

        case X:
          pressed = gamepad.x && !lastX;
          lastX = gamepad.x;
          break;

        case Y:
          pressed = gamepad.y && !lastY;
          lastY = gamepad.y;
          break;

        case LEFT_STICK:
          pressed = gamepad.left_stick_button && !lastLeftStick;
          lastLeftStick = gamepad.left_stick_button;
          break;

        case RIGHT_STICK:
          pressed = gamepad.right_stick_button && !lastRightStick;
          lastRightStick = gamepad.right_stick_button;
          break;

        case LEFT_BUMPER:
          pressed = gamepad.left_bumper && !lastLeftBumper;
          lastLeftBumper = gamepad.left_bumper;
          break;

        case RIGHT_BUMPER:
          pressed = gamepad.right_bumper && !lastRightBumper;
          lastRightBumper = gamepad.right_bumper;
          break;

          case OPTIONS:
              pressed = gamepad.options && !lastOptions;
              lastOptions = gamepad.options;
              break;

          case SHARE:
              pressed = gamepad.share && !lastShare;
              lastShare = gamepad.share;
              break;

          case HOME:
              pressed = gamepad.ps && !lastHome;
              lastHome = gamepad.ps;
              break;
      }

      return pressed;
    }

    // Get if a button is long-pressed, returns true while the button is not released (does not currently support left/right stick)
    public boolean longPress(Button button) {
      boolean longPressed = false;
      switch (button) {
        case A:
          if (!gamepad.a) lastNotATime = runtime.time();
          longPressed = gamepad.a && (runtime.time() - lastNotATime > longPressTime);
          break;

        case B:
          if (!gamepad.b) lastNotBTime = runtime.time();
          longPressed = gamepad.b && (runtime.time() - lastNotBTime > longPressTime);
          break;

        case X:
          if (!gamepad.x) lastNotXTime = runtime.time();
          longPressed = gamepad.x && (runtime.time() - lastNotXTime > longPressTime);
          break;

        case Y:
          if (!gamepad.y) lastNotYTime = runtime.time();
          longPressed = gamepad.y && (runtime.time() - lastNotYTime > longPressTime);
          break;

          case OPTIONS:
              if (!gamepad.options) lastNotOptionTime = runtime.time();
              longPressed = gamepad.options && (runtime.time() - lastNotOptionTime > longPressTime);
              break;

          case SHARE:
              if (!gamepad.share) lastNotShareTime = runtime.time();
              longPressed = gamepad.share && (runtime.time() - lastNotShareTime > longPressTime);
              break;

      }
      return longPressed;
    }
}
