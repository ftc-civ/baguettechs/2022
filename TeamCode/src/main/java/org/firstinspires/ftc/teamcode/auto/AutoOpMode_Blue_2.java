package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@Autonomous(name="AutoOpMode Blue W", group="Main")
public class AutoOpMode_Blue_2 extends AutoOpMode {

    public AutoOpMode_Blue_2(){
        super(Programm.BLUE_W);
    }
}
