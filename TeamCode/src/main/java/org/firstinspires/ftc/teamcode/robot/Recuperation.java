package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;

public class Recuperation {

    private ElapsedTime runtime;

    // Initialize motors
    private DcMotor ticksMotor;

    private double recupPower = 0;

    private Telemetry telemetry;

    public Recuperation(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor ticksMotorGlobal) {

        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        runtime = globalRuntime;

        // Assign motors from ManualOpMode
        ticksMotor = ticksMotorGlobal;

        ticksMotor.setDirection(DcMotor.Direction.FORWARD);

    }


    // Turns off all motors, full stop
    public void reset(boolean includeTurn) {
        recupPower= 0;
    }
    public void reset() {
        reset(true);
    }

    /* GAMEPAD CONTROL */
    public void marche() {
        recupPower  = 1;
     }
     public void inverse(){recupPower = -1;}
    public void arret() {
        recupPower  = 0;
    }
    /* GET INFO */

    public int getEncoder() {
        return ticksMotor.getCurrentPosition();
    }

    /* APPLY CHANGES */

    // Apply all changes made before
    public void apply() {
        ticksMotor.setPower(Range.clip(recupPower, -1.0, 1.0));


    }
}
