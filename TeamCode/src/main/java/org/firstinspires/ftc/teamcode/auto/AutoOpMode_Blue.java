package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;


import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.manual.ManualOpMode;

@Autonomous(name="AutoOpMode Blue C", group="Main")
public class AutoOpMode_Blue extends AutoOpMode {
    public AutoOpMode_Blue(){ super(Programm.BLUE_C);}
    }

