/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

//import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.manual.ManualOpMode;

//@Config
public class Movement
{

    ///////////////////////////////////////// Variables ////////////////////////////////////////////

    ////////////////////////////////// Public Variables //////////////////////////////////
    public static double frontLeftCoeff = 1;
    public static double frontRightCoeff = 1;
    public static double backLeftCoeff = 1;
    public static double backRightCoeff = 1;

    public static double turnSensitivity = 2; // Increasing sensitivity gives more priority to turning
    public static double frontSensitivity = 1;
    public static double sidewaysSensitivity = 1;

    public static double maxDpadSensitivity = 1; // How much the robot moves when dpad is used
    public static double timeToDpadFront = 3;
    public static double timeToDpadSideways = 2;
    public static double dpadMarginFront = 0.2;
    public static double dpadMarginSideways = 0.3;

    public enum Vitesses {RAPIDE,LENT,PRECISION};
    public Vitesses vitesse = Vitesses.RAPIDE;

    public enum Brake {TRUE, FALSE}
    public Brake brake = Brake.TRUE;

    ////////////////////////////////// Private variables //////////////////////////////////
    private float turn = 0f;
    private ElapsedTime runtime;
    // Initialize motors
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive, ticksMotor;
    private double frontLeftPower = 0;
    private double frontRightPower = 0;
    private double backLeftPower = 0;
    private double backRightPower = 0;
    private boolean isReduced = false;
    private Telemetry telemetry;
    private double firstTurnTicks;
    private boolean rotateLock;
    private double ticksAfterFullRotation = 2920; // TODO: Measure this with
    private double turnAngle, turnSpeed; // Angle in degrees
    private double dpadTime; // The time the dpad has been pressed, to progressively ramp up spee
    private boolean buttonpressed3 = false;

    private boolean isTriggered = false;
    private boolean isTriggered2 = false;

    private boolean telemetry_movement_on = true;
    public boolean motorsEnabled = true;
    ////////////////////////////////// Constructors //////////////////////////////////
    public Movement(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR, boolean isReducedMain) {


        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        runtime = globalRuntime;

        isReduced = isReducedMain;

        // Assign motors from ManualOpMode
        frontLeftDrive  = FL; frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive = FR; frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive  = BL;  backLeftDrive.setDirection(DcMotor.Direction.REVERSE);
        backRightDrive = BR;  backRightDrive.setDirection(DcMotor.Direction.FORWARD);

        frontRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        backLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        frontLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            ticksMotor = backLeftDrive;
    }

    ///////////////////////////////////////// METHODS /////////////////////////////////////////
    public void telemetry_movement(){
        Telemetry.Item movement =telemetry.addLine()
                .addData("Movement", "")
                .addData("Motors", motorsEnabled ? "enabled" : "disabled")
                .addData("Right Front", frontRightPower)
                .addData("Left Front", frontLeftPower)
                .addData("Right Rear", backRightPower)
                .addData("Left Rear", backLeftPower);
        if (telemetry_movement_on == false) {
            telemetry.removeItem(movement);
        }
    }


    /* MOVEMENT CALCULATIONS */

    // Move in a direction indicated by the first two coordinates, and turn
    public void move(double front, double sideways, double turn) {
        backLeftPower  += front + sideways*backLeftCoeff - turn;
        backRightPower += front - sideways*backRightCoeff + turn;
        frontRightPower  += front + sideways*frontRightCoeff + turn; // ou 0.2?
        frontLeftPower  += front - sideways*frontLeftCoeff - turn;
    }

    // Move with an angle and a speed instead
    public void polarMove(double angle, double speed, double turn) {
        move(
                Math.sin(angle) * speed,
                Math.cos(angle) * speed,
                turn
        );
    }

    // Add extra rotation
    public void rotate(double angleInDegrees, double speed) {

        rotateLock = true;
        firstTurnTicks = ticksMotor.getCurrentPosition();
        turnAngle = angleInDegrees; turnSpeed = speed;
    }

    // Add rotation lock
    public void putBrake(){
        if (buttonpressed3 == true) {
            frontRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            backRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            backLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            frontLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
            buttonpressed3 = false;
        } else {
            frontRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
            backRightDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
            backLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
            frontLeftDrive.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.FLOAT);
            buttonpressed3 = true;
        }
    }


    public void keepRotating() {
        //telemetry.addData("Rotating", firstTurnTicks + turnAngle / 360 * ticksAfterFullRotation);
        //telemetry.addData("Rotating", ticksMotor.getCurrentPosition());
        if (rotateLock) {
            if (turnAngle > 0) {
                if (firstTurnTicks + turnAngle / 360 * ticksAfterFullRotation > ticksMotor.getCurrentPosition()) {
                    move(0, 0, -turnSpeed);
                } else
                    rotateLock = false;
            } else {
                if (firstTurnTicks - turnAngle / 360 * ticksAfterFullRotation < ticksMotor.getCurrentPosition()) {
                    move(0, 0, turnSpeed);
                } else
                    rotateLock = false;
            }
        }
    }
    // Check if rotation lock is enabled
    public boolean checkRotating() {
        return rotateLock;
    }

    // Turns off all motors, full stop
    public void reset(boolean includeTurn) {
        frontLeftPower = 0;
        frontRightPower = 0;
        backLeftPower = 0;
        backRightPower = 0;
        if (includeTurn)
            turn = 0f;
    }
    public void reset() {
        reset(true);
    }

    /* GAMEPAD CONTROL */


    // Translates the robot with the left joystick
    public void joystickTranslate(Gamepad gamepad) {

        double sideways = vitesse == Vitesses.LENT ? gamepad.left_stick_x / 2 : vitesse == Vitesses.PRECISION ? gamepad.left_stick_x / 4 :gamepad.left_stick_x;
        double front = vitesse == Vitesses.LENT ? gamepad.left_stick_y / 2 : vitesse == Vitesses.PRECISION ? gamepad.left_stick_y / 4 :gamepad.left_stick_y;
        double turn = vitesse  == Vitesses.LENT ? gamepad.right_stick_x / 2 : vitesse == Vitesses.PRECISION ? gamepad.right_stick_x / 4 :gamepad.right_stick_x;
        // Uncomment for gradual speed increase
        if (Math.abs(sideways) < 0.1) {
            sideways = 0;
        } else if (Math.abs(sideways) > 0.1 || Math.abs(sideways) < 0.7) {
            sideways = 0.83*sideways+0.02;
        } else if (Math.abs(sideways) >= 0.7 || Math.abs(sideways) < 0.9) {
            sideways = 2*sideways-0.8;
        } else if (Math.abs(sideways) >=0.9) {
            sideways = 1;
        } else {
            sideways = 0;
        }

        if (Math.abs(front) < 0.4) {
            front = 0;
        } else if (Math.abs(front) > 0.1 || Math.abs(front) < 0.7) {
            front = 0.83*front+0.02;
        } else if (Math.abs(front) >= 0.7 || Math.abs(front) < 0.9) {
            front = 2*front-0.8;
        } else if (Math.abs(front) >=0.9) {
            front = 1;
        } else {
            front = 0;
        }


        if (Math.abs(turn) < 0.1) {
            turn = 0;
        } else if (Math.abs(turn) > 0.1 || Math.abs(turn) < 0.7) {
            turn = 0.83*turn+0.02;
        } else if (Math.abs(turn) >= 0.7 || Math.abs(turn) < 0.9) {
            turn = 2*turn-0.8;
        } else if (Math.abs(turn) >=0.9) {
            turn = 1;
        } else {
            turn = 0;
        }

        move(
                front,
                sideways,
                turn
        );


    }


    // Move slowly with the dpad
    public void dpadTranslate(Gamepad gamepad) {
            if (!(gamepad.dpad_left || gamepad.dpad_right)) {
                dpadTime = runtime.time();
            } // Allows us to get the time the dpad was pressed, so we can progressively augment the speed of the robot
            double time = runtime.time() - dpadTime;
            double sidewaysTime = time / timeToDpadSideways;
            sidewaysTime += dpadMarginSideways;
            if (vitesse == Vitesses.PRECISION) {
                maxDpadSensitivity = 0.2;
            } else if (vitesse == Vitesses.LENT) {
                maxDpadSensitivity = 0.4;
            } else {
                maxDpadSensitivity = 1;
            }

            if (sidewaysTime > maxDpadSensitivity || sidewaysTime <= maxDpadSensitivity) sidewaysTime = maxDpadSensitivity;
            move(
                    0,
                    (gamepad.dpad_right ? sidewaysTime : 0) - (gamepad.dpad_left ? sidewaysTime : 0),
                    0
            );

                if (gamepad.dpad_up == true){
                    if (isTriggered == false){
                        if (vitesse == Vitesses.PRECISION) {
                            vitesse = Vitesses.RAPIDE;
                        }
                        else {vitesse = Vitesses.PRECISION;
                        }
                    }
                    isTriggered = true;
                }
                if (gamepad.dpad_up == false){
                    isTriggered = false;
                }
                if (gamepad.dpad_down ==true){
                    if (isTriggered2 == false) {
                        if (vitesse == Vitesses.LENT) {
                            vitesse = Vitesses.RAPIDE;
                        }
                        else {vitesse = Vitesses.LENT;}
                    }
                    isTriggered2 = true;
                }
                if (gamepad.dpad_down ==false){
                    isTriggered2 = false;
                }

            }

    /* GET INFO */

    public int getEncoder() {
        return frontLeftDrive.getCurrentPosition();
    }

    /* APPLY CHANGES */

    // Apply all changes made before
    public void apply() {
        frontLeftDrive.setPower(Range.clip(frontLeftPower, -1.0, 1.0));
        frontRightDrive.setPower(Range.clip(frontRightPower, -1.0, 1.0));
        backLeftDrive.setPower(Range.clip(backLeftPower, -1.0, 1.0));
        backRightDrive.setPower(Range.clip(backRightPower, -1.0, 1.0));

    }
}
