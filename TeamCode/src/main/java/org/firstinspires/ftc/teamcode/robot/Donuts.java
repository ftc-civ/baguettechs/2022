/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.hardware.camera.Camera;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;

import java.util.List;

public class Donuts {
    private ElapsedTime runtime;

    private Telemetry telemetry;

    private TouchSensor donutEntryDetector;
    private TouchSensor donutExitDetector;

    private boolean lastDonutEntryDetector;
    private boolean lastDonutExitDetector;
    private int donuts;

    // TENSORFLOW STUFF
    private static final String TFOD_MODEL_ASSET = "UltimateGoal.tflite";
    private static final String LABEL_FIRST_ELEMENT = "Quad";
    private static final String LABEL_SECOND_ELEMENT = "Single";
    private static final String VUFORIA_KEY =
            "AeR4Ufv/////AAABmSp+W7zrm0pWhA6GMhExjldWqRUlKMRyJSF9/NFxur3/ODgvpBoN9r5lsuZcdL14xnyvEtHRI5rgNAjpLrP4WK8XIMPsfpGpWKz3sk2tqcCZbNB2Z4Pgl/lqjvsEugzW3pwyJKVvLw1ndgrPSKjGjDySNf+aeVSnpenWRMBEKxRlqrTpzxtPyM/sUz0o+JJpxpWO2PUARj6Vtzifg1dqjz+07hMR5PjJMVtS8dPi7jc8INS/m4JSefls+PtybcpjhvulYTRPfOgZIEx9kqVpq4wUH8Yx+QIvKwkieyJq581KluslmbtO705kmuE5WCfoHLw/ugG5XCGYM44nLXsQjjBXEhFhVkkFgMsMwNH+EPKq";
    private VuforiaLocalizer vuforia;
    private TFObjectDetector tfod;

    public Donuts(Telemetry globalTelemetry, ElapsedTime globalRuntime, TouchSensor globalDonutEntryDetector, TouchSensor globalDonutExitDetector, WebcamName camera, int viewId) {
        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;
        runtime = globalRuntime;

        // Assign servos
        donutEntryDetector = globalDonutEntryDetector;
        donutExitDetector = globalDonutExitDetector;

        // Tensorflow
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters();
        parameters.vuforiaLicenseKey = VUFORIA_KEY;
        parameters.cameraName = camera; // hardwareMap.get(WebcamName.class, "Webcam 1");
        vuforia = ClassFactory.getInstance().createVuforia(parameters);
        int tfodMonitorViewId = viewId; // hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        TFObjectDetector.Parameters tfodParameters = new TFObjectDetector.Parameters(tfodMonitorViewId);
        tfodParameters.minResultConfidence = 0.8f;
        tfod = ClassFactory.getInstance().createTFObjectDetector(tfodParameters, vuforia);
        tfod.loadModelFromAsset(TFOD_MODEL_ASSET, LABEL_FIRST_ELEMENT, LABEL_SECOND_ELEMENT);
    }

    public void activate() {
        tfod.activate();
    }

    public void deactivate() {
        tfod.deactivate();
    }

    public int get() {
        return donuts;
    }

    public int update() {
        if (donutEntryDetector.isPressed() && !lastDonutEntryDetector) {
            donuts += 1;
        }
        lastDonutEntryDetector = donutEntryDetector.isPressed();

        if (donutExitDetector.isPressed() && !lastDonutExitDetector) {
            donuts -= 1;
        }
        return donuts;
    }

    public enum CameraDonuts {
        NONE,
        SINGLE,
        QUAD,
        UNSURE
    }

    public CameraDonuts checkCamera() {
        List<Recognition> updatedRecognitions = tfod.getUpdatedRecognitions();
        if (updatedRecognitions != null && !updatedRecognitions.isEmpty()) {
            telemetry.addData("Object Detected", updatedRecognitions.get(0).getLabel());
            // step through the list of recognitions and display boundary info.
            if (updatedRecognitions.get(0).getConfidence() < 0.2) {
                return CameraDonuts.UNSURE;
            }
            if (updatedRecognitions.get(0).getLabel() == "Quad") {
                return Donuts.CameraDonuts.QUAD;
            }
            if (updatedRecognitions.get(0).getLabel() == "Single") {
                return Donuts.CameraDonuts.SINGLE;
            }

        }
        return CameraDonuts.NONE;
    }
}
