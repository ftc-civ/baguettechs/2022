package org.firstinspires.ftc.teamcode.auto;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

@Autonomous(name= "AutoOpMode Red W", group="Main")
public class AutoOpMode_Red_2 extends AutoOpMode {

    public AutoOpMode_Red_2(){
        super(Programm.RED_W);
    }

}
