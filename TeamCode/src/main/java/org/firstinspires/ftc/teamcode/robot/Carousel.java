package org.firstinspires.ftc.teamcode.robot;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;

import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.manual.ManualOpMode;

public class Carousel {
    ///////////////////////////////////////// Variables ////////////////////////////////////////////
    ////////////////////////////////// Private Variables //////////////////////////////////
    private ElapsedTime runtime;
    private Telemetry telemetry;
    private DcMotor carouselMotor;

    private double carouselPower = 0;
    private boolean cbuttonpressed = false;

    ////////////////////////////////// Constructors //////////////////////////////////
    public Carousel (Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor GlobalCarousel) {
        telemetry = globalTelemetry;
        runtime = globalRuntime;

        // Assign motors from ManualOpMode
        carouselMotor = GlobalCarousel;
        carouselMotor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        carouselMotor.setDirection(DcMotor.Direction.FORWARD);
    }

    ///////////////////////////////////////// METHODS /////////////////////////////////////////
    // Turns off all motors, full stop
    public void reset(boolean includeTurn) {
        carouselPower= 0;
    }
    public void reset() {
        reset(true);
    }


    /* GAMEPAD CONTROL */

    /////////////////////// ManualOpmode ///////////////////
    public void marche() {
        if (cbuttonpressed == false) {
            carouselPower  = 0.1;
            cbuttonpressed = true;
        } else {
            carouselPower  = 0;
            cbuttonpressed = false;
        }
    }

    public void inverse() {
        if (cbuttonpressed == false) {
            carouselPower = -0.1;
            cbuttonpressed = true;
        } else {
            carouselPower = 0;
            cbuttonpressed = false;
        }
    }

    public void arret() {
        carouselPower  = 0;
    }

    ///////////////// AutoOpMode /////////////////////////

    public void work(){
        carouselPower  = 0.1;
        carouselMotor.setPower(Range.clip(carouselPower, -1.0, 1.0));
    }

    public void workent(){
        carouselPower  = -0.1;
        carouselMotor.setPower(Range.clip(carouselPower, -1.0, 1.0));
    }

    public void stop(){
        carouselPower  = 0.0;
        carouselMotor.setPower(Range.clip(carouselPower, -1.0, 1.0));
    }


    /* GET INFO */

    public int getEncoder() {
        return carouselMotor.getCurrentPosition();
    }


    /* APPLY CHANGES */

    // Apply all changes made before
    public void apply() {
        carouselMotor.setPower(Range.clip(carouselPower, -1.0, 1.0));
    }
}

