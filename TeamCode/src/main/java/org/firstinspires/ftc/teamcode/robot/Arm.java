/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

// import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

// @Config
public class Arm
{
    public static double gripOpen = 0.4;
    public static double gripClosed = 0.7;
    public static double armOpen = 0.27;
    public static double armClosed = 0.70;
    public static double armStart = 0.10;

    private boolean gripState = true; // True for open, false for closed
    private boolean armState = true; // 0 for start, 1 for up, 2 for down
    private boolean armInit = true;

    private Servo grip, arm;

    private Telemetry telemetry;

    public Arm(Telemetry globalTelemetry, Servo gripServo, Servo armServo) {
        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        // Assign servos
        arm = armServo; arm.setDirection(Servo.Direction.FORWARD);
        grip = gripServo; grip.setDirection(Servo.Direction.FORWARD);

    }

    /* MANUAL CONTROL */
    // Changes arm between up and down position
    public void toggleArm() {
        armInit = false;
        armState = !armState;
        apply();
    }

    // Changes grip between open and closed position
    public void toggleGrip() {
        gripState = !gripState;
        apply();
    }

    // Sets arm position
    public void setArm(boolean state) {
        armInit = false;
        armState = state;
        apply();
    }

    // Sets grip position
    public void setGrip(boolean state) {
        gripState = state;
        apply();
    }


    /* UTILS */
    // Sets position of servos
    private void apply() {
      arm.setPosition(armInit ? armStart : (armState ? armOpen : armClosed));
      grip.setPosition(gripState ? gripOpen : gripClosed);
      telemetry.addData("Arm grip", gripState ? "open" : "closed");
      telemetry.addData("Arm arm", armState ? "open" : "closed");
    }
}
